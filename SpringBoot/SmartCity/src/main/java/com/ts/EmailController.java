// EmailController.java
package com.ts;

import org.apache.http.protocol.HTTP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.EmailService;
import com.model.APIResponseObject;

import java.util.*;
import static java.util.Map.Entry;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
public class EmailController {
    
	@Autowired
    private EmailService emailService;
	
	public Map<String,String> emailOtpMap = new HashMap<>();
    
    @PostMapping("/sendMail")
    public ResponseEntity<Object> sendEmail(@RequestBody Map<String , Object> requestBody){
    	
    	 
    	String toEmail = (String)requestBody.get("email");
    	String subject = (String) requestBody.get("subject");
    	String body = (String) requestBody.get("content");
    	
    	try{
    		
    		emailService.sendEmail(toEmail, subject, body);
    		return ResponseEntity.ok(true);
    		
//    		return ResponseEntity.ok("Email sent successfully");

    	}catch(Throwable t){
    		 return ResponseEntity.ok(false);
//    		t.printStackTrace();
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
//            		.body("Internal Server Error: " + t.getLocalizedMessage());
            
    	}
    	
    }
    
    @PostMapping("/sendOtp")
    public ResponseEntity<Object> sendOtp(@RequestBody Map<String, String> request) {
        
    	String toEmail = (String)request.get("email");
    	String subject = "KitchenMaker - Email Verification";
    	

    	String bodyContent = "Yout Email verification otp for KitchenMaker reg.\n"
    			+ "is : ";
        // Generate a random 6-digit OTP
        String body = String.format("%06d", new Random().nextInt(1000000));

        // Store the OTP in memory or in a database (for simplicity, using a Map here)
        emailOtpMap.put(toEmail, body);
        
        
        try{
    		
    		emailService.sendEmail(toEmail, subject, bodyContent + body );
    		
//    		Map<String,Object> responseObject = new HashMap<>();
//    		responseObject.put("message","Otp sent sucessfully!");
//    		responseObject.put("success",true);

    		String  msg = "Otp Sent successfully";
    		return ResponseEntity.ok(true);
    			

    	}
        catch(Throwable t){
    		t.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            		.body("Internal Server Error: " + t.getLocalizedMessage());
            
    	}
    }
    @GetMapping("/fetchOtp/{email}/{givenOtp}")
    public ResponseEntity<Boolean> fetchOtp(@PathVariable String email, @PathVariable String givenOtp) {
        System.out.println("in fetchOtp ::" + email + "," + givenOtp);
        System.out.println("otp map :: " + emailOtpMap);
        
        String storedOtp = emailOtpMap.get(email);
        
        if (storedOtp != null && storedOtp.equals(givenOtp)) {
            return ResponseEntity.ok(true);
        } else {
            return ResponseEntity.ok(false);
        }
    }


}
