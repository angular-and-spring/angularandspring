// RecipesController.java
package com.ts;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import com.dao.RecipesDAO;
import com.model.Recipes;

@RestController
public class RecipesController {
    
    @Autowired
    RecipesDAO recipesDAO;

    @GetMapping("/getAllRecipes")
    public List<Recipes> getAllRecipes() {
        return recipesDAO.getAllRecipes();
    }

    @GetMapping("/getRecipesByName/{recipeName}")
    public List<Recipes> getRecipesByName(@PathVariable String recipeName) {
        return recipesDAO.getRecipesByName(recipeName);
    }
}
