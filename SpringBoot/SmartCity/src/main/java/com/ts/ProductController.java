package com.ts;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ProductDAO;
import com.model.Product;

@RestController
public class ProductController {

	//Dependency Injection for ProductDAO
	@Autowired
	ProductDAO prodDAO;
	
	
	@GetMapping("getProductByCategory/{category}")
	public List<Product> getProductByCategory(@PathVariable("category") String prodCategory) {	
	    return prodDAO.getProductByCategory(prodCategory);		
	}
	
	@RequestMapping("getAllProducts")
	public List<Product> getAllProducts() {	
		return prodDAO.getAllProducts();
	}	
	
	
	
	@GetMapping("getProductById/{id}")
	public Product getProductById(@PathVariable("id") int productid) {	
	    return prodDAO.getProductById(productid);		
	}
	@PostMapping("addProduct")
	public Product addProduct(@RequestBody Product product) {		
	    return prodDAO.addProduct(product);
	}
	
	@PutMapping("updateProduct")
	public Product updateProduct(@RequestBody Product product) {		
	    return prodDAO.updateProduct(product);
	}	
	
	@DeleteMapping("deleteProductById/{id}")
	public String deleteProductById(@PathVariable("id") int productId) {	
		prodDAO.deleteProductById(productId);
		return "Product Deleted Successfully";
	}
	@GetMapping("getProductByName/{name}")
	public Product getProductByName(@PathVariable("name") String prodName) {	
	    return prodDAO.getProductByName(prodName);		
	}


}