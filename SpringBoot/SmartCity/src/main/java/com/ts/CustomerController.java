// CustomerController.java
package com.ts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import com.dao.CustomerDAO;
import com.model.Customer;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class CustomerController {
    
    @Autowired
    private CustomerDAO custDAO;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @GetMapping("getAllCustomers")
    public List<Customer> getAllCustomers() {
        return custDAO.getAllCustomers();
    }

    @PostMapping("registerCustomer")
    public Customer registerCustomer(@RequestBody Customer cust) {
        // Encrypt the password before saving to the database
        String encryptedPassword = bCryptPasswordEncoder.encode(cust.getCustPassword());
        cust.setCustPassword(encryptedPassword);
        return custDAO.registerCust(cust);
    }

    @GetMapping("custLogin/{emailId}/{password}")
    public Customer custLogin(@PathVariable("emailId") String emailId, @PathVariable("password") String password) {
        // Call DAO to check login with the provided email and password
        return custDAO.custLogin(emailId, password);
    }
    
    @PostMapping("changepassword")
    public String changePassword(@RequestBody Map<String, String> request) {
        String email = request.get("email");
        String newPassword = request.get("newPassword");

        Customer customer = custDAO.changePassword(email, newPassword);

        if (customer != null) {
            return "{\"status\":\"success\",\"message\":\"Password changed successfully\"}";
        } else {
            return "{\"status\":\"error\",\"message\":\"Failed to change password\"}";
        }
    }
    
    @DeleteMapping("/deleteCustomerById/{custId}")
    public void deleteCustomerById(@PathVariable int custId) {
    	System.out.println(custId);
        custDAO.deleteCustomerById(custId);
    }
    
    @PutMapping("updateCustomer")
	public Customer updateCustomer(@RequestBody Customer customer) {		
	    return custDAO.updateCustomer(customer);
	}

    
    
    
    
    
    
}
