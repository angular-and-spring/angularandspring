package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
	
	@Query("from Product where prodName = :pname")
	Product findByName(@Param("pname") String prodName);
	
	@Query("from Product where prodCategory = :pcategory")
	List<Product> findByCategory(@Param("pcategory") String prodCategory);
}