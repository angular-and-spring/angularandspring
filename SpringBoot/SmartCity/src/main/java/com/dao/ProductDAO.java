package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Product;

@Service
public class ProductDAO {
	
	//Dependency Injection for ProductRepository: can be implemented using @Autowried Annotation
	
	@Autowired
	ProductRepository prodRepo;

	
	public List<Product> getAllProducts() {
		return prodRepo.findAll();
	}
	public Product getProductById(int productId) {
	    try {
	        Product product = new Product(0, "Product Not Found!!!", 0.0, " ", "");
	        return prodRepo.findById(productId).orElse(product);
	    } catch (Exception e) {
	        e.printStackTrace(); // Log or print the exception
	        return new Product(0, "Error retrieving product", 0.0, "", "");
	    }
	}
	
	public Product addProduct(Product product) {
	    return prodRepo.save(product);
	}

	public Product updateProduct(Product product) {
	    return prodRepo.save(product);
	}


public void deleteProductById(int productId) {
    prodRepo.deleteById(productId);
}

public Product getProductByName(String prodName) {
    return prodRepo.findByName(prodName);
}

public List<Product> getProductByCategory(String prodCategory) {
	// TODO Auto-generated method stub
	return prodRepo.findByCategory(prodCategory);
}

}