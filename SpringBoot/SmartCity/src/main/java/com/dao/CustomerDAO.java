// CustomerDAO.java
package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.Customer;

@Service
public class CustomerDAO {
    
    @Autowired
    private CustomerRepository custRepo;

    public List<Customer> getAllCustomers() {
        return custRepo.findAll();
    }

    public Customer registerCust(Customer cust) {
        return custRepo.save(cust);
    }

    public Customer custLogin(String emailId, String password) {
        // Retrieve the user based on the email
        Customer customer = custRepo.findBycustEmail(emailId).orElse(null);

        // Check if the user exists and the password matches
        if (customer != null && passwordMatches(password, customer.getCustPassword())) {
            return customer;
        } else {
            return null;
        }
    }

    private boolean passwordMatches(String rawPassword, String encodedPassword) {
        // Use BCryptPasswordEncoder's matches method
        return new BCryptPasswordEncoder().matches(rawPassword, encodedPassword);
    }
   
    
    public Customer changePassword(String email, String newPassword) {
        Customer customer = custRepo.findBycustEmail(email).orElse(null);

        if (customer != null) {
            // Use the correct class name: BCryptPasswordEncoder
        	
            String encryptedPassword = new BCryptPasswordEncoder().encode(newPassword);
       
            
            customer.setCustPassword(encryptedPassword);
            return custRepo.save(customer);
        } else {
            return null; // Handle case where the customer is not found
        }
    }

    public void deleteCustomerById(int empId) {
		custRepo.deleteById(empId);
	}
    
    public Customer updateCustomer(Customer customer) {
	    return custRepo.save(customer);
	}
    


    
    
}
