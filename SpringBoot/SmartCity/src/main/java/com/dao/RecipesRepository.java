// RecipesRepository.java
package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.model.Recipes;

@Repository
public interface RecipesRepository extends JpaRepository<Recipes, Integer> {

    List<Recipes> findByRecipeName(String recipeName);
}
