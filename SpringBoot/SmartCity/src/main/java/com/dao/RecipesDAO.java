// RecipesDAO.java
package com.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.model.Recipes;

@Service
public class RecipesDAO {

    @Autowired
    RecipesRepository recipesRepo;

    public List<Recipes> getAllRecipes() {
        return recipesRepo.findAll();
    }

    public List<Recipes> getRecipesByName(String recipeName) {
        return recipesRepo.findByRecipeName(recipeName);
    }
}
