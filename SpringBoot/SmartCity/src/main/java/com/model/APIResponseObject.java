package com.model;


public class APIResponseObject {
	String message;
	Boolean success;
	int status;
	public APIResponseObject(String message, Boolean success, int status) {
		super();
		this.message = message;
		this.success = success;
		this.status = status;
	}
	
	@Override
	public String toString() {
		return "APIResponseObject [message=" + message + ", success=" + success + ", status=" + status + "]";
	}
	

}
