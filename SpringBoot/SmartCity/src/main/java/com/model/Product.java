package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Product {
	    @Id
	    @GeneratedValue
	    private int prodId;
	    private String prodName;
	    private double prodPrice;
	    private String prodCategory;
	    private String prodImage;
	    
	    
	    
	    
	    
		public Product() {
			super();
			// TODO Auto-generated constructor stub
		}
		
		
		
		public Product(int prodId, String prodName, double prodPrice, String prodCategory, String prodImage) {
			super();
			this.prodId = prodId;
			this.prodName = prodName;
			this.prodPrice = prodPrice;
			this.prodCategory = prodCategory;
			this.prodImage = prodImage;
		}



		public int getProdId() {
			return prodId;
		}
		public void setProdId(int prodId) {
			this.prodId = prodId;
		}
		public String getProdName() {
			return prodName;
		}
		public void setProdName(String prodName) {
			this.prodName = prodName;
		}
		public double getProdPrice() {
			return prodPrice;
		}
		public void setProdPrice(double prodPrice) {
			this.prodPrice = prodPrice;
		}
		public String getProdCategory() {
			return prodCategory;
		}
		public void setProdCategory(String prodCategory) {
			this.prodCategory = prodCategory;
		}
		public String getProdImage() {
			return prodImage;
		}
		public void setProdImage(String prodImage) {
			this.prodImage = prodImage;
		}
	    
	    
	    
}
