package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Recipes {
    
	@Id
    @GeneratedValue
    private int recipeId;

    private String recipeName;

    // Use a delimiter (e.g., comma) to separate ingredients in a single string
    private String ingredients;

    private String ytLink;

    // Constructors, getters, and setters

    public Recipes() {
        // default constructor
    }

    public Recipes(int recipeId, String recipeName, String ingredients, String ytLink) {
        this.recipeId = recipeId;
        this.recipeName = recipeName;
        this.ingredients = ingredients;
        this.ytLink = ytLink;
    }


	

	public int getRecipeId() {
		return recipeId;
	}

	public void setRecipeId(int recipeId) {
		this.recipeId = recipeId;
	}

	public String getRecipeName() {
		return recipeName;
	}

	public void setRecipeName(String recipeName) {
		this.recipeName = recipeName;
	}

	
	public String getIngredients() {
		return ingredients;
	}

	public void setIngredients(String ingredients) {
		this.ingredients = ingredients;
	}

	public String getYtLink() {
		return ytLink;
	}

	public void setYtLink(String ytLink) {
		this.ytLink = ytLink;
	}
    
    
    

   
}
